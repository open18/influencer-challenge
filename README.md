# Influencer Challenge

El Challenge consiste en listar Blog Posts desde un GraphQL API.

## Tech Stack

- ReactJS
- Typescript
- GraphQL
- Styles: se puede utilizar pure CSS o cualquier libraria que te ayude a maquetar (TailwindCSS, Bootstrap, etc)

## Demo

https://master.d3cfqkykl99z4b.amplifyapp.com/
