import Blog from '../components/Blog';

import { gql, useMutation, useQuery } from '@apollo/client';
import { useState } from 'react';
import Loading from '../components/Loading';
import swal from 'sweetalert';

const ALL_POSTS = gql`
query($options: PageQueryOptions) {
    posts(options: $options) {
      data {
        id
        title
        body
      }
      meta {
        totalCount
      }
    }
  }  
`;

const UPDATE_POST = gql`
mutation (
    $id: ID!,
    $input: UpdatePostInput!
  ) {
    updatePost(id: $id, input: $input) {
      id
      body
    }
  }
`;

const DELETE_POST = gql`
mutation (
    $id: ID!
  ) {
    deletePost(id: $id)
  }
`;

const ADD_POST = gql`
mutation (
    $input: CreatePostInput!
  ) {
    createPost(input: $input) {
      id
      title
      body
    }
  }
`;

type BlogType = {
    id: number,
    title: string,
    body: string
}

type MainType = {
    paginationSize: number
}

const Main = ({ paginationSize }: MainType) => {

    const [page, setPage] = useState(1);
    const [response, setResponse] = useState<any>();

    const [updBlog] = useMutation(UPDATE_POST);
    const [delBlog] = useMutation(DELETE_POST);
    const [addBlog] = useMutation(ADD_POST);

    const { loading, error } = useQuery(ALL_POSTS, {
        variables: {
            "options": {
                "paginate": {
                    page,
                    limit: paginationSize,
                }
            }
        },
        onCompleted: setResponse
    });

    if (loading || response === undefined) return <Loading />
    if (error) return <p>Error :(</p>;


    const { totalCount } = response.posts.meta;
    const posts = response.posts.data;

    const totalPages = Math.ceil((totalCount / paginationSize));



    const updateBlog = (blog: BlogType) => {
        updBlog({
            variables: {
                id: blog.id,
                input: { title: blog.title, body: blog.body }
            }
        });
        swal("Genial", "Datos actualizados correctamente", "success");
    }

    const deleteBlog = (blog: BlogType) => {
        delBlog({
            variables: { id: blog.id }
        });

        //delete locally
        const newPosts = posts.filter((i: any) => i.id !== blog.id);
        setResponse({
            posts: {
                data: newPosts,
                meta: response.posts.meta,
            }
        });
        swal("Genial", "Dato eliminado", "success");
    }

    const createBlog = (blog: BlogType) => {
        addBlog({
            variables: { input: { title: blog.title, body: blog.body } }
        });

        //add locally
        const newPosts = [...posts];
        newPosts.push({
            ...blog,
            id: posts.length + 1
        });

        setResponse({
            posts: {
                data: newPosts,
                meta: response.posts.meta,
            }
        });
        swal("Genial", "Dato eliminado", "success");
    }


    return (
        <div className="container">
            <div className="c-blog">
                <h5>Pagina {page}</h5>
            </div>
            {posts.map((item: BlogType) => (
                <Blog
                    key={item.id}
                    title={item.title}
                    body={item.body}
                    id={item.id}
                    updateBlog={updateBlog}
                    deleteBlog={deleteBlog}
                />
            ))}
            <div className="c-blog">
                {(page !== 1) && (
                    <div className="left">
                        <button className="btn-actions text-blue" onClick={() => setPage(page - 1)}>Previous</button>
                    </div>
                )}
                {(totalPages !== page) && (
                    <div className="right">
                        <button className="btn-actions text-blue" onClick={() => setPage(page + 1)}>Next</button>
                    </div>
                )}
            </div>
            <div className="c-blog">
                <h2 className="text-blue">Create new post</h2>
            </div>
            <Blog
                title={""}
                body={""}
                isNew={true}
                createBlog={createBlog}
            />
        </div>
    );
};


export default Main;