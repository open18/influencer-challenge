import React, { useState } from "react";

type BlogProps = {
    id?: number,
    title: string,
    body: string,
    deleteBlog?: Function,
    updateBlog?: Function,
    createBlog?: Function,
    isNew?: Boolean
}

const Blog = ({ id, title, body, updateBlog, deleteBlog, isNew, createBlog }: BlogProps) => {

    const [edit, setEdit] = useState(false);

    const [titleEdited, setTitleEdited] = useState(title);
    const [bodyEdited, setBodyEdited] = useState(body);

    const updBlog = (e: React.FormEvent) => {
        e.preventDefault();
        setEdit(false);
        if (isNew) {
            setTitleEdited("");
            setBodyEdited("");
            if (createBlog)
                createBlog({ id, title: titleEdited, body: bodyEdited });
        } else {
            if (updateBlog)
                updateBlog({ id, title: titleEdited, body: bodyEdited });
        }

    }

    const delBlog = () => {
        if (deleteBlog)
            deleteBlog({ id, title, body });
    }

    if (edit || isNew) {
        return (
            <form method="post" onSubmit={updBlog}>
                <div className="c-blog">
                    <input
                        type="text"
                        value={titleEdited}
                        onChange={e => setTitleEdited(e.target.value)}
                        required
                    />
                    <input
                        type="text"
                        value={bodyEdited}
                        onChange={e => setBodyEdited(e.target.value)}
                        required
                    />
                    <div className="right">
                        <button className="gradient-button">{isNew ? "CREATE" : "UPDATE"}</button>
                    </div>
                    <hr></hr>
                </div>
            </form>
        );
    } else {
        return (
            <div className="c-blog">
                <h3>{id}: {titleEdited}</h3>
                <p>{bodyEdited}</p>
                <div className="c-btns-actions">
                    <button className="btn-actions text-blue" onClick={() => setEdit(true)}>Editar</button>
                    <button className="btn-actions text-red" onClick={delBlog}>Delete</button>
                </div>
                <hr />
            </div>
        );
    }


};

export default Blog;