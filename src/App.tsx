import "./App.css";
import Main from "./views/Main";


const App = () => {

    return (
        <Main paginationSize={10} />
    );
};

export default App;
